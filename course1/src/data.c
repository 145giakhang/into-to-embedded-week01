/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file data.c
 * @brief Abstraction of data manipulations
 *
 * This implementation file provides an abstraction of manipulating data.
 *
 * @author Alex Fosdick
 * @date April 1 2017
 *
 */
#include <stdint.h>
#include <stdlib.h>
#include "memory.h"
#include "data.h"
#include "course1.h"

/***********************************************************
 Function Definitions
***********************************************************/

uint8_t my_itoa(int32_t data, uint8_t *ptr, uint32_t base) {
    /* consider absolute value of number */
    uint32_t n = abs(data);
    uint8_t *str = (uint8_t*) reserve_words( DATA_SET_SIZE_W );
    uint8_t len = 0;
    uint8_t i = 0;
    while (n) {
        uint32_t r = n % base;

        if (r >= 10) {
            *(str + i) = 65 + (r - 10);
        } else {
            *(str + i) = 48 + r;
        }
        i++;
        len++;

        n = n / base;
    }

    /* if n == 0 => i == 0 */
    if (i == 0) {
        *(str + i) = '0';
        i++;
        len++;
    }

    /* if data is negative => leading '-' */
    if (data < 0) {
        *(str + i) = '-';
        i++;
        len++;
    }

    /* null terminate character */
    *(str + i) = '\0';
    len++;

    /* copy to output ptr */
    ptr = my_memmove(str, ptr, len);
    ptr = my_reverse(ptr, len - 1);
    //int j;
    //for (j=0; j < len; j++) 
    //    printf("%c\n", *(ptr+j));
    return len;
}

int32_t my_atoi(uint8_t *ptr, uint8_t digits, uint32_t base) {
    int32_t res = 0;
    int32_t i = 0;
    int32_t sign = 1;

    /* get sign */
    if (*(ptr+i) == '-') {
        sign = -1;
        i++;
    }

    /* exclude the null terminate character \0 */
    int32_t j = digits - 2;
    /* init b = 1 = base^0 */
    int32_t b = 1;
    /* loop from backward */
    while (j >= i) {
        if (base == 16) { 
            /* if hex character is 0-9 */
            if (*(ptr + j) >= '0' && *(ptr + j) <= '9') {
                res += (*(ptr + j) - '0') * b;
            /* else A-F */
            } else {
                uint32_t num;
                switch (*(ptr + j)) {
                    case 'A': case 'a': num = 10; break;
                    case 'B': case 'b': num = 11; break;
                    case 'C': case 'c': num = 12; break;
                    case 'D': case 'd': num = 13; break;
                    case 'E': case 'e': num = 14; break;
                    case 'F': case 'f': num = 15; break;
                }

                res += num * b;
            }
        } else {
            res += (*(ptr + j) - '0') * b;
        }
        j--;
        b *= base;
    }

    return (res * sign);
}
