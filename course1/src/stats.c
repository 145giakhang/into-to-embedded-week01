/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief Analyze and report maximum, minimum, mean and median value of the data set
 *
 * Analyze an array of unsigned char data items and report analytics on the maximum, minimum, mean, and median of the data set,
 * reorder this data set from large to small. All statistics should be rounded down to the nearest integer. 
 * After analysis and sorting is done, print that data to the screen in nicely formatted presentation.
 *
 * @author Gia Khang Lam
 * @date Nov 23, 2019
 *
 */



#include <stdio.h>
#include "stats.h"
#include "platform.h"

/* Size of the Data Set */
#define SIZE (40)

//void main() {
//	unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
//								114, 88,   45,  76, 123,  87,  25,  23,
//								200, 122, 150, 90,   92,  87, 177, 244,
//								201,   6,  12,  60,   8,   2,   5,  67,
//								7,  87, 250, 230,  99,   3, 100,  90};
//
//	/* Other Variable Declarations Go Here */
//	unsigned char median, mean, max, min;
//	/* Statistics and Printing Functions Go Here */
//
//	// firstly, sort the given array
//	sort_array(test, SIZE);
//	// find median, mean, max, min values
//	median = find_median(test, SIZE);
//	mean   = find_mean(test, SIZE);
//	max    = find_maximum(test, SIZE);
//	min    = find_minimum(test, SIZE);
//
//	// print out the statistics
//	printf("Array sorted in descending order:\n");
//	print_array(test, SIZE);
//	printf("\n");
//	print_statistics(median, mean, max, min);
//}

/* Add other Implementation File Code Here */
void print_statistics(unsigned char median, unsigned char mean, 
	unsigned char max, unsigned char min) {
	printf("========= Statistic Analyze ==========\n");
	printf("Median value: \t%u\n", median);
	printf("Mean value:   \t%u\n", mean);
	printf("Maximum value:\t%u\n", max);
	printf("Minimum value:\t%u\n", min); 
}

void print_array(const unsigned char *arr, unsigned char length) {
#ifdef VERBOSE
    int i;
	for (i = 0; i < length; i++) {
		PRINTF("%u ", arr[i]);
	}
	PRINTF("\n");
#endif
}

unsigned char find_median(const unsigned char *arr, unsigned char length) {
	unsigned char median = 0;
	// if length is even, then median is average of 2 middle elements
	if (length % 2 == 0) {
		median = (arr[(length-1)/2] + arr[length/2]) / 2;
	// else if length is odd, then median is the middle element
	} else {
		median = arr[length/2];
	}

	return median;
}

unsigned char find_mean(const unsigned char *arr, unsigned char length) {
	int sum = 0;
	sum = sum_array(arr, length);
	return (unsigned char)(sum / length);
}

unsigned char find_maximum(const unsigned char *arr, unsigned char length) {
	return arr[0];
}

unsigned char find_minimum(const unsigned char *arr, unsigned char length) {
	return arr[length-1];
}

void sort_array(unsigned char *arr, unsigned char length) {
	int i, curr, j;
	// loop from the element at index 1 since the first element already sorted
	for (i = 1; i < length; i++) {
		// current value = arr[i]
		curr = arr[i];
		j = i - 1;

		// loop backward in the already sorted arr[0..j]
		// while j still valid and element arr[j] is smaller than current value
		// then move value arr[j] back 1 step
		while (j >= 0 && curr > arr[j]) {
			arr[j+1] = arr[j];
			j--;
		}
		// assign current value to its right position
		arr[j+1] = curr;
	}
}

int sum_array(const unsigned char *arr, unsigned char length) {
	int i; 
	int sum = 0;
	for (i = 0; i < length; i++) {
		sum += arr[i];
	}
	return sum;
}
