/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file data.h
 * @brief Abstraction of data manipulations
 *
 * This header file provides an abstraction of manipulating data. 
 *
 * @author Alex Fosdick
 * @date April 1 2017
 *
 */
#ifndef __DATA_H__
#define __DATA_H__

/**
 * @brief Convert integer to ASCII
 *
 * Given an integer 'data', a pointer 'ptr' and 'base', 
 * this will convert 'data' to corresponding ASCII c-string base 'base',
 * and store into 'ptr'. 
 * Ex: my_itoa(1234, ptr, 10) => ptr = "1234\0"
 *
 * @param data The integer need to be converted
 * @param ptr The holder for the converted c-string
 * @param base The base to convert to (2, 10, 16) 
 *
 * @return Length of the converted string (including null terminator and negative sign (if any))
 */
uint8_t my_itoa(int32_t data, uint8_t *ptr, uint32_t base);

/**
 * @brief Convert ASCII to integer
 *
 * Given a character string 'ptr', its length 'digits' and its base 'base', 
 * this will convert 'ptr' to corresponding signed 32-bit integer,
 * Ex: my_atoi("1234", 4, 10) =>  return 1234
 *
 * @param ptr The character string need to be converted
 * @param digits The number of digits in character set
 * @param base The base to support (2, 10, 16) 
 *
 * @return The converted 32-bit signed integer
 */
int32_t my_atoi(uint8_t *ptr, uint8_t digits, uint32_t base);

#endif /* __DATA_H__ */
