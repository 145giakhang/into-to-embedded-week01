/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief Define declaration of all required functions
 *
 * Define declaration of all required functions, including their functionalities,
 * inputs and outputs
 *
 * @author Gia Khang Lam
 * @date Nov 23, 2019
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/* Add Your Declarations and Function Comments here */ 

/**
 * @brief Prints the statistics of an array
 *
 * A function that prints the statistics of an array 
 * including minimum, maximum, mean, and median.
 * 
 * @param median median value
 * @param mean mean value
 * @param max maximum value
 * @param min minimum value
 */
void print_statistics(unsigned char median, unsigned char mean, unsigned char max, unsigned char min);


/**
 * @brief Prints the array to the screen
 *
 * Given an array of data and a length, prints the array to the screen
 *
 * @param arr the given array of data
 * @param length size of the array
 */
void print_array(const unsigned char *arr, unsigned char length);


/**
 * @brief Returns the median value of an array
 *
 * Given an array of data and a length, returns the median value,
 * rounded down to nearest integer
 *
 * @param arr descending-order sorted array of data
 * @param length size of the array
 * 
 * @return Return an unsigned char which is the median value of the array
 */
unsigned char find_median(const unsigned char *arr, unsigned char length);


/**
 * @brief Returns the mean value of an array
 *
 * Given an array of data and a length, returns the mean value,
 * rounded down to nearest integer
 *
 * @param arr descending-order sorted array of data
 * @param length size of the array
 * 
 * @return Return an unsigned char which is the mean value of the array
 */
unsigned char find_mean(const unsigned char *arr, unsigned char length);


/**
 * @brief Returns the maximum value of an array
 *
 * Given an array of data and a length, returns the maximum value,
 * rounded down to nearest integer
 *
 * @param arr descending-order sorted array of data
 * @param length size of the array
 * 
 * @return Return an unsigned char which is the maximum value of the array
 */
unsigned char find_maximum(const unsigned char *arr, unsigned char length);


/**
 * @brief Returns the minimum value of an array
 *
 * Given an array of data and a length, returns the minimum value,
 * rounded down to nearest integer
 *
 * @param arr descending-order sorted array of data
 * @param length size of the array
 * 
 * @return Return an unsigned char which is the minimum value of the array
 */
unsigned char find_minimum(const unsigned char *arr, unsigned char length);


/**
 * @brief Sort an array in descending order
 *
 * Given an array of data and a length, sort the array 
 * from largest to smallest using Insertion sort algorithm.
 * (The zeroth Element should be the largest value, and 
 * the last element (n-1) should be the smallest value)
 *
 * @param arr the given array of data
 * @param length size of the array
 */
void sort_array(unsigned char *arr, unsigned char length);


/**
 * @brief Returns sum of all elements in the given array
 *
 * Given an array of data and a length, returns the sum of all elements
 * in the given array, rounded down to nearest integer 
 *
 * @param arr the given array of data
 * @param length size of the array
 * 
 * @return Return an integer which is the sum of all values in the array
 */
int sum_array(const unsigned char *arr, unsigned char length);

#endif /* __STATS_H__ */
